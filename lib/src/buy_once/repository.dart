import 'package:preeti_bhojan_daily/src/common/models/deliverable.dart';
import 'package:preeti_bhojan_daily/src/common/models/product.dart';

abstract class OptionRepository {
  Future<List<Option>> optionsFor(Product product);
}

class StubOptionRepository implements OptionRepository {
  @override
  Future<List<Option>> optionsFor(Product product) => Future.delayed(
        Duration(seconds: 1),
        () => [
          Option('500 ML', 23.5),
          Option('1 L', 47),
          Option('1.5L', 70.5),
          Option('2 L', 94),
        ],
      );
}
