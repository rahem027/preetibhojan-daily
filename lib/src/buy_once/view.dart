import 'package:async_builder/async_builder.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:preeti_bhojan_daily/src/buy_once/repository.dart';
import 'package:preeti_bhojan_daily/src/common/cart.dart';
import 'package:preeti_bhojan_daily/src/common/models/deliverable.dart';
import 'package:preeti_bhojan_daily/src/common/models/product.dart';
import 'package:preeti_bhojan_daily/src/common/widgets/quantity_picker.dart';

class BuyOnce extends StatefulWidget {
  final Product product;
  final OptionRepository optionRepository;
  final Cart cart;

  const BuyOnce(this.product, this.optionRepository, this.cart);

  @override
  _BuyOnceState createState() => _BuyOnceState();
}

class _BuyOnceState extends State<BuyOnce> {
  late Future<void> _future;

  @override
  void initState() {
    super.initState();
    _future = _init();
  }

  final _options = <Option>[];

  Future<void> _init() async {
    _options.clear();
    _options.addAll(await widget.optionRepository.optionsFor(widget.product));
  }

  final _quantityFor = <Option, int>{};

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(AppLocalizations.of(context)!.buyOnce),
      ),
      body: AsyncBuilder<void>(
        future: _future,
        waiting: (context) => Center(child: CircularProgressIndicator()),
        builder: (context, _) => ListView(
          padding: const EdgeInsets.all(8),
          children: [
            Container(
              height: 150,
              child: Image.network(
                widget.product.imageUrl,
                fit: BoxFit.fitHeight,
              ),
            ),
            Center(
              child: Text(
                widget.product.name,
                style: Theme.of(context).textTheme.headline6,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 16),
              child: Text(
                AppLocalizations.of(context)!.options,
                style: Theme.of(context)
                    .textTheme
                    .headline6!
                    .copyWith(fontSize: 19),
              ),
            ),
            Column(
              children: _options.map(
                (option) {
                  final optionSelected = _quantityFor.containsKey(option);
                  final onTap = () {
                    setState(() {
                      if (optionSelected) {
                        _quantityFor.remove(option);
                      } else {
                        _quantityFor[option] = 1;
                      }
                    });
                  };

                  return InkWell(
                    onTap: onTap,
                    child: Row(
                      children: [
                        Checkbox(
                          value: optionSelected,
                          onChanged: (_) {
                            onTap();
                          },
                        ),
                        SizedBox(width: 16),
                        Text(option.name),
                        Spacer(),
                        if (optionSelected)
                          QuantityPicker(
                            listener: (quantity) {
                              setState(() {
                                _quantityFor[option] = quantity;
                              });
                            },
                          ),
                      ],
                    ),
                  );
                },
              ).toList(),
            ),
            Column(
              children: [
                ElevatedButton(
                  child: Text(AppLocalizations.of(context)!.buy),
                  style: ElevatedButton.styleFrom(minimumSize: Size(120, 38)),
                  onPressed: _buy,
                ),
                ElevatedButton(
                  child: Text(AppLocalizations.of(context)!.addToCart),
                  style: ElevatedButton.styleFrom(minimumSize: Size(120, 38)),
                  onPressed: _addToCart,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  void _addToCart() {
    if (_quantityFor.isEmpty) {
      ScaffoldMessenger.of(context)
        ..hideCurrentSnackBar()
        ..showSnackBar(
          SnackBar(
            content: Text(AppLocalizations.of(context)!.pleaseSelectAnOption),
          ),
        );
      return;
    }

    widget.cart.deliverables[widget.product] = _quantityFor;

    ScaffoldMessenger.of(context)
      ..hideCurrentSnackBar()
      ..showSnackBar(
        SnackBar(
          content: Text(AppLocalizations.of(context)!.itemAddedToCart),
        ),
      );
    Navigator.of(context).pop();
  }

  void _buy() {
    if (_quantityFor.isEmpty) {
      ScaffoldMessenger.of(context).hideCurrentSnackBar();
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text(AppLocalizations.of(context)!.pleaseSelectAnOption),
      ));
      return;
    }
  }
}
