import 'package:async_builder/async_builder.dart';
import 'package:flutter/material.dart';
import 'package:preeti_bhojan_daily/src/buy_once/repository.dart';
import 'package:preeti_bhojan_daily/src/common/models/days.dart';
import 'package:preeti_bhojan_daily/src/common/models/deliverable.dart';
import 'package:preeti_bhojan_daily/src/common/models/product.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:preeti_bhojan_daily/src/common/widgets/quantity_picker.dart';
import 'package:preeti_bhojan_daily/src/schedule/advanced_schedule.dart';

class StandardSchedule extends StatefulWidget {
  final Product product;
  final OptionRepository optionRepository;

  const StandardSchedule(this.product, this.optionRepository);

  @override
  _StandardScheduleState createState() => _StandardScheduleState();
}

enum _DeliveryFrequency {
  everyday,
  alternateDays,
  custom,
}

class _StandardScheduleState extends State<StandardSchedule> {
  late Future<void> _future;

  @override
  void initState() {
    super.initState();
    _future = _init();
  }

  late final _options;
  Option? _selectedOption;

  Future<void> _init() async {
    _options = await widget.optionRepository.optionsFor(widget.product);
  }

  _DeliveryFrequency? _deliveryFrequency;
  List<Day>? _daysRequired;
  int _quantity = 1;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(AppLocalizations.of(context)!.schedule),
      ),
      body: AsyncBuilder<void>(
        future: _future,
        waiting: (context) => Center(
          child: CircularProgressIndicator(),
        ),
        builder: (BuildContext context, _) {
          return ListView(
            padding: const EdgeInsets.all(8),
            children: [
              Container(
                height: 150,
                child: Image.network(
                  widget.product.imageUrl,
                  fit: BoxFit.fitHeight,
                ),
              ),
              Center(
                child: Text(
                  widget.product.name,
                  style: Theme.of(context).textTheme.headline6,
                ),
              ),
              DropdownButtonFormField<_DeliveryFrequency>(
                items: [
                  DropdownMenuItem(
                    child: Text(AppLocalizations.of(context)!.everyday),
                    value: _DeliveryFrequency.everyday,
                  ),
                  DropdownMenuItem(
                    child: Text(AppLocalizations.of(context)!.alternateDays),
                    value: _DeliveryFrequency.alternateDays,
                  ),
                  DropdownMenuItem(
                    child: Text(AppLocalizations.of(context)!.custom),
                    value: _DeliveryFrequency.custom,
                  ),
                ],
                decoration: InputDecoration(
                  labelText: AppLocalizations.of(context)!.howOftenDoYouWant,
                ),
                onChanged: (_DeliveryFrequency? newDeliveryFrequency) {
                  // switch (newDeliveryFrequency) {
                  //   case _DeliveryFrequency.mondayToFriday:
                  //     setState(() {
                  //       _deliveryFrequency = _DeliveryFrequency.mondayToFriday;
                  //       _daysRequired = Day.mondayToFriday;
                  //     });
                  //     break;
                  //   case _DeliveryFrequency.mondayToSaturday:
                  //     setState(() {
                  //       _deliveryFrequency =
                  //           _DeliveryFrequency.mondayToSaturday;
                  //       _daysRequired = Day.mondayToSaturday;
                  //     });
                  //     break;
                  //   case _DeliveryFrequency.custom:
                  //     setState(() {
                  //       _deliveryFrequency = _DeliveryFrequency.custom;
                  //       if (_daysRequired == null) {
                  //         _daysRequired = [];
                  //       }
                  //     });
                  //     break;
                  //   case null:
                  //     throw StateError('delivery frequency should not be null');
                  //   case _DeliveryFrequency.everyday:
                  //     _deliveryFrequency = _DeliveryFrequency.everyday;
                  //     _daysRequired = Day.values;
                  //     break;
                  // }
                },
              ),
              if (_deliveryFrequency == _DeliveryFrequency.custom)
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    children: Day.values.map(
                      (day) {
                        final daySelected =
                            _daysRequired?.contains(day) ?? false;
                        final onTap = () {
                          setState(() {
                            if (daySelected) {
                              _daysRequired?.remove(day);
                            } else {
                              _daysRequired?.add(day);
                            }
                          });
                        };

                        return InkWell(
                          onTap: onTap,
                          child: Row(
                            children: [
                              Checkbox(
                                value: daySelected,
                                onChanged: (_) {
                                  onTap();
                                },
                              ),
                              SizedBox(width: 16),
                              Text(day.day),
                            ],
                          ),
                        );
                      },
                    ).toList(),
                  ),
                ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      AppLocalizations.of(context)!.options,
                      style: Theme.of(context)
                          .textTheme
                          .headline6!
                          .copyWith(fontSize: 19),
                    ),
                    ..._options.map(
                      (option) {
                        final onTap = () {
                          setState(() {
                            _selectedOption = option;
                          });
                        };

                        return InkWell(
                          onTap: onTap,
                          child: Row(
                            children: [
                              Radio<Option>(
                                value: option,
                                onChanged: (_) {
                                  onTap();
                                },
                                groupValue: _selectedOption,
                              ),
                              SizedBox(width: 16),
                              Text(option.name),
                            ],
                          ),
                        );
                      },
                    ),
                  ],
                ),
              ),
              QuantityPicker(
                listener: (quantity) {
                  setState(() {
                    _quantity = quantity;
                  });
                },
              ),
              Row(
                children: [
                  Flexible(
                    fit: FlexFit.tight,
                    child: Center(
                      child: ElevatedButton(
                        child: Text(AppLocalizations.of(context)!.advanced),
                        onPressed: () {
                          Navigator.of(context).push(
                            MaterialPageRoute(
                              builder: (context) => AdvancedSchedule(
                                widget.product,
                                _options,
                              ),
                            ),
                          );
                        },
                      ),
                    ),
                  ),
                  Flexible(
                    fit: FlexFit.tight,
                    child: Center(
                      child: ElevatedButton(
                        child: Text(AppLocalizations.of(context)!.schedule),
                        onPressed: () {},
                      ),
                    ),
                  ),
                ],
              ),
            ],
          );
        },
      ),
    );
  }
}
