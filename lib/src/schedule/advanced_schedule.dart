import 'package:async_builder/async_builder.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:preeti_bhojan_daily/src/buy_once/repository.dart';

import '../common/models/days.dart';
import '../common/models/deliverable.dart';
import '../common/models/product.dart';
import '../common/widgets/quantity_picker.dart';

class AdvancedSchedule extends StatefulWidget {
  final Product product;
  final OptionRepository optionRepository;

  const AdvancedSchedule(this.product, this.optionRepository);

  @override
  _AdvancedScheduleState createState() => _AdvancedScheduleState();
}

class _AdvancedScheduleState extends State<AdvancedSchedule> {
  final _schedule = <Day, Map<Option, int>>{};

  late Future<void> _future;

  @override
  void initState() {
    super.initState();
    _future = _init();
  }

  late final _options;

  Future<void> _init() async {
    _options = await widget.optionRepository.optionsFor(widget.product);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(AppLocalizations.of(context)!.advancedSchedule),
      ),
      body: AsyncBuilder<void>(
        future: _future,
        waiting: (context) => Center(child: CircularProgressIndicator()),
        builder: (context, _) => ListView(
          children: [
            Container(
              height: 150,
              child: Image.network(
                widget.product.imageUrl,
                fit: BoxFit.fitHeight,
              ),
            ),
            Center(
              child: Text(
                widget.product.name,
                style: Theme.of(context).textTheme.headline6,
              ),
            ),
            Column(
              children: Day.values.map(
                (day) {
                  final daySelected = _schedule.containsKey(day);
                  final onTap = () {
                    setState(() {
                      if (daySelected) {
                        _schedule.remove(day);
                      } else {
                        _schedule[day] = {};
                      }
                    });
                  };

                  return Column(
                    children: [
                      InkWell(
                        onTap: onTap,
                        child: Row(
                          children: [
                            Checkbox(
                              value: daySelected,
                              onChanged: (_) {
                                onTap();
                              },
                            ),
                            SizedBox(width: 16),
                            Text(day.day),
                          ],
                        ),
                      ),
                      if (daySelected)
                        Padding(
                          padding: const EdgeInsets.only(left: 16),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                AppLocalizations.of(context)!.options,
                                style: Theme.of(context)
                                    .textTheme
                                    .headline6!
                                    .copyWith(fontSize: 19),
                              ),
                              ..._options.map(
                                (option) {
                                  final _quantityFor = _schedule[day]!;
                                  final optionSelected =
                                      _quantityFor.containsKey(option);
                                  final onTap = () {
                                    setState(() {
                                      if (optionSelected) {
                                        _quantityFor.remove(option);
                                      } else {
                                        _quantityFor[option] = 1;
                                      }
                                    });
                                  };

                                  return InkWell(
                                    onTap: onTap,
                                    child: Row(
                                      children: [
                                        Checkbox(
                                          value: optionSelected,
                                          onChanged: (_) {
                                            onTap();
                                          },
                                        ),
                                        SizedBox(width: 16),
                                        Text(option.name),
                                        Spacer(),
                                        if (optionSelected)
                                          QuantityPicker(
                                            listener: (quantity) {
                                              setState(() {
                                                _quantityFor[option] = quantity;
                                              });
                                            },
                                          ),
                                      ],
                                    ),
                                  );
                                },
                              )
                            ],
                          ),
                        )
                    ],
                  );
                },
              ).toList(),
            ),
            Center(
              child: ElevatedButton(
                child: Text(AppLocalizations.of(context)!.schedule),
                onPressed: () {},
              ),
            ),
          ],
        ),
      ),
    );
  }
}
