import 'package:preeti_bhojan_daily/src/home/models.dart';

import '../common/models/product.dart';

abstract class ProductsRepository {
  Future<List<Product>> products(Category category);
}

class StubProductsRepository implements ProductsRepository {
  @override
  Future<List<Product>> products(Category c) => Future.delayed(
        Duration(seconds: 1),
        () => [
          Product(
            1,
            'Amul Milk',
            'https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse1.mm.bing.net%2Fth%3Fid%3DOIP.ThF-qoP2YigksKCLwqKQywHaIq%26pid%3DApi%26h%3D160&f=1',
            Subcategory(1, 'Milk'),
          ),
          Product(
            2,
            'Chitale Milk',
            'https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse1.mm.bing.net%2Fth%3Fid%3DOIP.XcX9OzD9EawhSGLkXLESQwAAAA%26pid%3DApi%26h%3D160&f=1',
            Subcategory(1, 'Milk'),
          ),
          Product(
            3,
            'Mother Dairy Milk',
            'https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse1.mm.bing.net%2Fth%3Fid%3DOIP.5gpVnLLMYCyNagmjpKctHgHaHa%26pid%3DApi%26h%3D160&f=1',
            Subcategory(1, 'Milk'),
          ),
          Product(
            4,
            'Amul Lassi',
            'https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse1.mm.bing.net%2Fth%3Fid%3DOIP.PHjDqSIKGLzVL-K-XDxsmAHaHa%26pid%3DApi%26h%3D160&f=1',
            Subcategory(2, 'Lassi'),
          ),
          Product(
            5,
            'Chitale Lassi',
            'https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse1.mm.bing.net%2Fth%3Fid%3DOIP.0CtA--uQoNkEafGnN-s63AHaFj%26pid%3DApi%26h%3D160&f=1',
            Subcategory(2, 'Lassi'),
          ),
          Product(
            6,
            'Mother Dairy Lassi',
            'https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse1.mm.bing.net%2Fth%3Fid%3DOIP.O_q_0eSMM9oMC1MbjCMt6AHaHa%26pid%3DApi%26h%3D160&f=1',
            Subcategory(2, 'Lassi'),
          ),
        ],
      );
}
