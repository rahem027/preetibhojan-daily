import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:preeti_bhojan_daily/src/common/models/product.dart';

class FilterScreen extends StatefulWidget {
  final List<Subcategory> allFilters, enabledFilters;

  const FilterScreen(this.allFilters, this.enabledFilters);

  @override
  _FilterScreenState createState() => _FilterScreenState();
}

class _FilterScreenState extends State<FilterScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(AppLocalizations.of(context)!.filters),
      ),
      body: ListView(
        padding: const EdgeInsets.all(8.0),
        children: [
          Text(
            AppLocalizations.of(context)!.filters,
            style: Theme.of(context).textTheme.headline6,
          ),
          ...widget.allFilters.map(
            (filter) => Row(
              children: [
                Text(
                  filter.name,
                  style: TextStyle(fontSize: 18),
                ),
                Spacer(),
                Checkbox(
                  value: widget.enabledFilters.contains(filter),
                  onChanged: (bool? selected) {
                    if (selected == null) return;

                    setState(() {
                      if (selected) {
                        widget.enabledFilters.add(filter);
                      } else if (widget.enabledFilters.length > 1) {
                        widget.enabledFilters.remove(filter);
                      } else {
                        ScaffoldMessenger.of(context).showSnackBar(
                          SnackBar(
                            content: Text(AppLocalizations.of(context)!.atLeastOneFilterNeedsToBeEnabled),
                          ),
                        );
                      }
                    });
                  },
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
