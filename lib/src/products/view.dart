import 'package:async_builder/async_builder.dart';
import 'package:flutter/material.dart';
import 'package:preeti_bhojan_daily/src/home/models.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:preeti_bhojan_daily/src/products/repositories.dart';

import '../navigators.dart';
import 'filter_screen.dart';
import '../common/models/product.dart';

class ProductsScreen extends StatefulWidget {
  final Category category;
  final ProductsRepository repository;

  ProductsScreen(this.category, this.repository);

  @override
  _ProductsScreenState createState() => _ProductsScreenState();
}

class _ProductsScreenState extends State<ProductsScreen> {
  late Future<void> _future;

  @override
  void initState() {
    super.initState();
    _future = _init();
  }

  List<Product> _products = <Product>[];
  List<Subcategory> _allFilters = <Subcategory>[],
      _enabledFilters = <Subcategory>[];

  Future<void> _init() async {
    _products = await widget.repository.products(widget.category);

    _enableAllSubcategories();
  }

  void _enableAllSubcategories() {
    final subcategories = _products.map((p) => p.subcategory).toSet().toList();
    _allFilters = subcategories;
    _enabledFilters = [..._allFilters];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(AppLocalizations.of(context)!.preetiBhojan),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.filter_alt),
        onPressed: () async {
          await Navigator.of(context).push(
            MaterialPageRoute(
              builder: (context) => FilterScreen(_allFilters, _enabledFilters),
            ),
          );

          /// refresh the new selected filters
          setState(() {});
        },
      ),
      body: AsyncBuilder<void>(
        future: _future,
        waiting: (context) => Center(child: CircularProgressIndicator()),
        builder: (context, _) => CustomScrollView(
          slivers: [
            SliverGrid.extent(
              maxCrossAxisExtent: 200,
              childAspectRatio: 0.72,
              children: _products
                  .where((p) => _enabledFilters.contains(p.subcategory))
                  .map<Widget>(
                    (product) => Card(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          children: [
                            Container(
                              height: 150,
                              child: Image.network(
                                product.imageUrl,
                                fit: BoxFit.fitWidth,
                              ),
                            ),
                            Text(
                              product.name,
                              style: Theme.of(context).textTheme.headline6,
                            ),
                            Container(
                              height: 35,
                              padding: const EdgeInsets.only(top: 4),
                              child: ElevatedButton(
                                child: Text(
                                    AppLocalizations.of(context)!.schedule),
                                onPressed: () {
                                  schedule(context, product);
                                },
                              ),
                            ),
                            Container(
                              height: 35,
                              padding: const EdgeInsets.only(top: 4),
                              child: TextButton(
                                child:
                                    Text(AppLocalizations.of(context)!.buyOnce),
                                onPressed: () {
                                  buyOnce(context, product);
                                },
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  )
                  .toList(),
            ),
            SliverList(
              delegate: SliverChildListDelegate([Container(height: 60)]),
            )
          ],
        ),
      ),
    );
  }
}
