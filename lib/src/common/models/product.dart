class Subcategory {
  final int id;
  final String name;

  Subcategory(this.id, this.name);

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Subcategory &&
          runtimeType == other.runtimeType &&
          id == other.id &&
          name == other.name;

  @override
  int get hashCode => id.hashCode ^ name.hashCode;
}

class Product {
  final int id;
  final String name, imageUrl;
  final Subcategory subcategory;

  Product(this.id, this.name, this.imageUrl, this.subcategory);

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Product &&
          runtimeType == other.runtimeType &&
          id == other.id &&
          name == other.name &&
          imageUrl == other.imageUrl &&
          subcategory == other.subcategory;

  @override
  int get hashCode =>
      id.hashCode ^
      name.hashCode ^
      imageUrl.hashCode ^
      subcategory.hashCode;
}