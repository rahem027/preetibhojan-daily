class Day {
  final int id;
  final String day;

  const Day._(this.id, this.day);

  static const Monday = Day._(1, 'Monday');
  static const Tuesday = Day._(2, 'Tuesday');
  static const Wednesday = Day._(3, 'Wednesday');
  static const Thursday = Day._(4, 'Thursday');
  static const Friday = Day._(5, 'Friday');
  static const Saturday = Day._(6, 'Saturday');
  static const Sunday = Day._(7, 'Sunday');

  static const values = [
    Monday,
    Tuesday,
    Wednesday,
    Thursday,
    Friday,
    Saturday,
    Sunday,
  ];

  static const mondayToFriday = [
    Monday,
    Tuesday,
    Wednesday,
    Thursday,
    Friday,
  ];

  static const mondayToSaturday = [
    Monday,
    Tuesday,
    Wednesday,
    Thursday,
    Friday,
    Saturday,
  ];

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Day &&
          runtimeType == other.runtimeType &&
          id == other.id &&
          day == other.day;

  @override
  int get hashCode => id.hashCode ^ day.hashCode;
}
