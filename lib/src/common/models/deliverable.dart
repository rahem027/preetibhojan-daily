import 'package:preeti_bhojan_daily/src/common/models/product.dart';

class Option {
  final String name;
  final double price;

  Option(this.name, this.price);

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Option &&
          runtimeType == other.runtimeType &&
          name == other.name &&
          price == other.price;

  @override
  int get hashCode => name.hashCode ^ price.hashCode;
}

class Deliverable {
  final Product product;
  final Map<Option, int> quantityFor;

  Deliverable(this.product, this.quantityFor);
}
