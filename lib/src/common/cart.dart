import 'models/deliverable.dart';
import 'models/product.dart';

class Cart {
  final deliverables = <Product, Map<Option, int>>{};
}

final cart = Cart();