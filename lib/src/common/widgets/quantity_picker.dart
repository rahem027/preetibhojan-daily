import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class QuantityPicker extends StatefulWidget {
  final ValueChanged<int> listener;

  const QuantityPicker({required this.listener});

  @override
  _QuantityPickerState createState() => _QuantityPickerState();
}

class _QuantityPickerState extends State<QuantityPicker> {
  int _quantity = 1;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text('${AppLocalizations.of(context)!.quantity}: '),
        IconButton(
          onPressed: _decreaseQuantity,
          icon: Icon(Icons.remove),
        ),
        Text('$_quantity'),
        IconButton(
          onPressed: _increaseQuantity,
          icon: Icon(Icons.add),
        ),
      ],
    );
  }

  void _decreaseQuantity() {
    if (_quantity == 1) {
      ScaffoldMessenger.of(context).hideCurrentSnackBar();
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text(AppLocalizations.of(context)!.minimumOneQuantityRequired),
      ));
      return;
    }

    setState(() {
      _quantity--;
      widget.listener(_quantity);
    });
  }

  void _increaseQuantity() {
    setState(() {
      _quantity++;
      widget.listener(_quantity);
    });
  }
}
