import 'package:async_builder/async_builder.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:preeti_bhojan_daily/src/navigators.dart';

import 'models.dart';
import 'repositories.dart';

class HomeScreen extends StatefulWidget {
  final CategoryRepository categoryRepository;

  const HomeScreen(this.categoryRepository);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  late Future<void> _future;

  final _categories = <Category>[];

  @override
  void initState() {
    super.initState();
    _future = _init();
  }

  Future<void> _init() async {
    try {
      _categories.addAll(await widget.categoryRepository.categories);
    } catch (error, stacktrace) {
      print('Caught error: $error');
      print(stacktrace);
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(error.toString()),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(AppLocalizations.of(context)!.preetiBhojan),
      ),
      body: AsyncBuilder<void>(
        future: _future,
        waiting: (context) => Center(child: CircularProgressIndicator()),
        builder: (context, _) => GridView.count(
          crossAxisCount: 3,
          children: _categories
              .map(
                (category) => InkWell(
                  child: Card(
                    elevation: 5,
                    child: Column(
                      children: [
                        Container(
                          child: Image.network(
                            category.photoUrl,
                            fit: BoxFit.fitWidth,
                          ),
                          height: 100,
                        ),
                        Text(category.name),
                      ],
                    ),
                  ),
                  onTap: () {
                    products(context, category);
                  },
                ),
              )
              .toList(),
        ),
      ),
    );
  }
}
