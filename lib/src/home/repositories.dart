import 'models.dart';

abstract class CategoryRepository {
  Future<List<Category>> get categories;
}

class StubCategoryRepository implements CategoryRepository {
  @override
  Future<List<Category>> get categories => Future.delayed(
        Duration(seconds: 1),
        () => [
          Category('1', 'Fruits',
              'https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse3.mm.bing.net%2Fth%3Fid%3DOIP.aJ1Lm4fEhn6WplApUSqUywHaE8%26pid%3DApi&f=1'),
          Category('2', 'Vegetables',
              'https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse4.mm.bing.net%2Fth%3Fid%3DOIP.2lS8HIXJ0ZKVZL83j655RQHaE8%26pid%3DApi&f=1'),
          Category('3', 'Milk',
              'https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse3.mm.bing.net%2Fth%3Fid%3DOIP.t6_zO0ObXBtRQHLd67E29QHaF_%26pid%3DApi&f=1')
        ],
      );
}
