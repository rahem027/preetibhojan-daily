class Category {
  final String id, name, photoUrl;

  Category(this.id, this.name, this.photoUrl);
}