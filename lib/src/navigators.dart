import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:preeti_bhojan_daily/src/buy_once/repository.dart';
import 'package:preeti_bhojan_daily/src/buy_once/view.dart';
import 'package:preeti_bhojan_daily/src/home/models.dart';
import 'package:preeti_bhojan_daily/src/products/repositories.dart';
import 'package:preeti_bhojan_daily/src/products/view.dart';

import 'common/cart.dart';
import 'common/models/product.dart';
import 'schedule/advanced_schedule.dart';
import 'schedule/standard_schedule.dart';

Future<T?> products<T>(BuildContext context, Category category) {
  return Navigator.of(context).push(
    MaterialPageRoute(
      builder: (_) => ProductsScreen(category, StubProductsRepository()),
    ),
  );
}


Future<T?> buyOnce<T>(BuildContext context, Product product) {
  return Navigator.of(context).push(
    MaterialPageRoute(
      builder: (_) => BuyOnce(product, StubOptionRepository(), cart),
    ),
  );
}


Future<T?> schedule<T>(BuildContext context, Product product) {
  return Navigator.of(context).push(
    MaterialPageRoute(
      builder: (_) => StandardSchedule(product, StubOptionRepository()),
    ),
  );
}
